var http = require('http');

var server = http.createServer(function (req, res) {
    var category = req.url;

    if(category == '/tech'){
        res.end("<html><body><h1>Tech Section</h1></body></html>");
    } else if(category == '/edu') {
        res.end("<html><body><h1>Education Section</h1></body></html>");
    } else {
        res.end("<html><body><h1>Hello World</h1></body></html>");
    }

})

server.listen(3000)