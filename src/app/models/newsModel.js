function News(connection) {
    this._connection = connection;
}

News.prototype.getNews = function(callback) {
    this._connection.query('SELECT * FROM news order by created_at desc', callback);
}

News.prototype.getNewsById = function(id_news, callback) {
    this._connection.query('SELECT * FROM news WHERE id = ' + id_news, callback);
}

News.prototype.saveNews = function(data, callback) {
    this._connection.query('INSERT INTO news SET ?', data, callback);
}

News.prototype.getLastFiveNews = function(callback) {
    this._connection.query('SELECT * FROM news order by created_at desc limit 5', callback)
}

module.exports = () => {
    return News
}