module.exports.index = function(app, req, res) {
    const connection = app.src.config.dbConnection();
    const newsModel = new app.src.app.models.newsModel(connection);

    newsModel.getLastFiveNews(function(error, result) {
        res.render('home/index', {news: result, error});
    })
}