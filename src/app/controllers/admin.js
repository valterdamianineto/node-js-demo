module.exports.form_add_news = function(app, req, res) {
    res.render('admin/formAddNews', {errors: [], data: {}})
}

module.exports.save_news = function(app, req, res) {
    const data = req.body;

    req.assert('title', 'Title is required').notEmpty()
    req.assert('subtitle', 'Subtitle is required').notEmpty()
    req.assert('content', 'Content is required').notEmpty()

    const errors = req.validationErrors()
    
    if(errors) {
        res.render('admin/formAddNews', { errors, data})
        return 
    }
    const connection =  app.src.config.dbConnection();
    const newsModel = new app.src.app.models.newsModel(connection);

    newsModel.saveNews(data, (error, result) => {
        res.redirect('/main')
    })
}