module.exports.list_news =  function(app, req, res) {
    const connection =  app.src.config.dbConnection();
    const newsModel = new app.src.app.models.newsModel(connection);

    newsModel.getNews((error, news) => {
        res.render('section/listNews', { news })
    })
}

module.exports.get_news = function(app, req, res) {
    const connection =  app.src.config.dbConnection();
    const newsModel = new app.src.app.models.newsModel(connection);
    const id_news = req.query.id_news;

    newsModel.getNewsById(id_news, (error, news) => {
        res.render('section/news', { news })
    })
}