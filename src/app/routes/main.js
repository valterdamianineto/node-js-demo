module.exports = (app) => {
    app.get('/main', (req, res) => {
        app.src.app.controllers.news.list_news(app, req, res);
    })

    app.get('/news', (req, res) => {
        app.src.app.controllers.news.get_news(app, req, res);
    })
}