module.exports = (app) => {
    app.get('/admin/add-news', (req, res) => {
        app.src.app.controllers.admin.form_add_news(app, req, res);
    })

    app.post('/admin/news/save', (req, res) => {
        app.src.app.controllers.admin.save_news(app, req, res);
    })
}