const express = require('express');
const consign = require('consign');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');

const app = express();
app.set('view engine', 'ejs');
app.set('views', './src/app/views');
app.use(express.static('./src/app/public'))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator())

consign()
.include('src/app/routes')
.then('src/config/dbConnection.js')
.then('src/app/models')
.then('src/app/controllers')
.into(app)

module.exports = app;