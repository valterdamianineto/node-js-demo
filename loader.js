const app = require('./src/config/server')

app.listen(3000, () => {
    console.log('Example app listening on port 3000!')
})